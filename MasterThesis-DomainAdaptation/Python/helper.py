import numpy as np
from numpy.linalg import inv
import scipy
import pandas as pd
import networkx as nx
import matplotlib.pyplot as plt
from sklearn import linear_model
import seaborn as sns
from data_simulator import *
# import subset_search from causal_transfer_learning\code\subset_search.py
from causal_transfer_learning.code import subset_search
from copy import deepcopy




def concatenate_data(data_list=[]):
    """Concatenates a list of dataframes.
    
    Args:
        data_list: list of dataframes
        
    Returns:
        df: concatenated dataframe"""
    int_history = []
    new_data_list = []
    int_nodes_history = []
    for data in data_list:
        new_data = deepcopy(data)
        new_data['env'] = data.env
        int_history.append(data.intervention_info)
        int_nodes_history.append(data.intervention_nodes_all)
        new_data_list.append(new_data)
    df = pd.concat(new_data_list)
    df.intervention_info = int_history
    df.int_nodes_history = int_nodes_history
    df.reset_index(drop=True, inplace=True)
    return df


def concatenate_data_with_hidden(data_list=[],hidden_indices=[]):
    """Concatenates a list of dataframes and delete hidden data."""
    int_history = []
    new_data_list = []
    int_nodes_history = []
    for data in data_list:
        new_data = deepcopy(data)
        new_data = new_data.drop(data.columns[hidden_indices], axis=1)
        new_data['env'] = data.env
        int_history.append(data.intervention_info)
        temp = deepcopy(data.intervention_nodes_all)
        # change all values in temp that is larger than the largest value in hidden_indices to the smallest value in hidden_indices
        temp=[min(hidden_indices) if x > max(hidden_indices) else x for x in temp]
        int_nodes_history.append(temp)
        new_data_list.append(new_data)
    df = pd.concat(new_data_list)
    df.intervention_info = int_history
    df.int_nodes_history = int_nodes_history
    df.reset_index(drop=True, inplace=True)
    return df



def df_to_dict(data_list=[]):
    """ Converts a list of dataframes to a dictionary of dataframes. """
    data_dict = {}
    for i in range(len(data_list)):
        data_dict[data_list[i].env] = (np.array(data_list[i].iloc[:,:-1]), np.array(data_list[i].iloc[:,-1]))
    return data_dict

def add_df_to_df(df, data_list=[]):
    """Adds a list of dataframes to a dataframe."""
    temp = df.intervention_info.copy() if df.intervention_info is not None else []
    new_data_list = []
    for data in data_list:
        new_data = data.copy()
        new_data['env'] = data.env
        new_data_list.append(new_data)
        temp.append(data.intervention_info)
    df = pd.concat([df] + new_data_list)
    df.intervention_info = temp
    df.reset_index(drop=True, inplace=True)
    return df

def add_df_to_dict( data_dict, data_list=[]):
    """ Adds a list of dataframes to a dictionary of dataframes. """
    for i in range(len(data_list)):
        data_dict[data_list[i].env] = (np.array(data_list[i].iloc[:,:-1]), np.array(data_list[i].iloc[:,-1]))
    return data_dict


def select_df_data(df_full, source_envs, target_envs):
    """ Selects train and test data as numpy.arrays from a dataframes based on the environment. df_full last column is the environment. y_train and y_test should be column vectors."""
    df_train = df_full[df_full.env.isin(source_envs)]
    df_test = df_full[df_full.env == target_envs]
    X_train = np.array(df_train.iloc[:,:-2])
    y_train = np.array(df_train.iloc[:,-2])[:,np.newaxis]
    X_test = np.array(df_test.iloc[:,:-2])
    y_test = np.array(df_test.iloc[:,-2])[:,np.newaxis]
    return X_train, y_train, X_test, y_test



def fit_linear_model(X_train, y_train, X_test, y_test, feature_subset=np.empty(0), regularization=None, alpha=None):
    """ Fit model with specified feature subset, or simply choose mean of y_train if the feature_subset is empty. Can choose regularization from None, 'lasso' and 'ridge'.

    Args:
        X_train: training data
        y_train: training labels
        X_test: test data
        y_test: test labels
        feature_subset: subset of features to use
        regularization: None, 'lasso' or 'ridge'
        alpha: regularization parameter
    
    Returns:
        pred: prediction of the model
    """
    if feature_subset.size == 0:
        # let pred be the mean of y_train as the same length as y_test
        pred = np.full(y_test.shape, np.mean(y_train))
    else:
        if regularization is None:
            regr = linear_model.LinearRegression() # default is to fit with intercept and without regularization
        elif regularization == 'lasso':
            if alpha is None:
                alpha = 1.0
            regr = linear_model.Lasso(alpha=alpha)
        elif regularization == 'ridge':
            if alpha is None:
                alpha = 1.0
            regr = linear_model.Ridge(alpha=alpha)
        else:
            raise ValueError('Regularization must be either None, "lasso" or "ridge"')
        regr.fit(X_train[:, feature_subset], y_train)
        pred = regr.predict(X_test[:, feature_subset])
    return pred

    
def MSE(y_test, pred):
    """ First flatten y_test and pred to 1D arrays, then calculate the mean squared error."""
    return np.mean((y_test.flatten() - pred.flatten())**2)

def add_hidden_nodes(adj_matrix, hidden_nodes):
    """ Add hidden nodes to the adj_matrix.
     
    Args:
        adj_matrix: adjacency matrix of the original graph
        hidden_nodes: list of tuples of hidden nodes, each tuple contains the children of the hidden node.
    """
    # get the total number of nodes
    p = adj_matrix.shape[0]
    n = len(hidden_nodes)
    # create a new adj_matrix with the new nodes, initialize to 0
    new_adj_matrix = np.zeros((p+n, p+n))
    # copy the weights for edges of x to the new matrix
    new_adj_matrix[:p-1, :p-1] = adj_matrix[:p-1, :p-1]
    # copy the weights for edges of y to the new matrix
    new_adj_matrix[-1, :p-1] = adj_matrix[-1,:-1]
    new_adj_matrix[:p-1, -1] = adj_matrix[:-1,-1]
    # add edges for hidden variables
    for i in range(len(hidden_nodes)):
        # new node index should be i+p
        for child in hidden_nodes[i]:
            # if child is Y
            if child == p-1:
                # add edge from i+p-1 to Y
                new_adj_matrix[i+p-1, n+p-1] = np.random.uniform(-3, -0.5) if np.random.rand() < 0.5 else np.random.uniform(0.5, 3)
            else:
                # add edge from i+p-1 to child
                new_adj_matrix[i+p-1, child] = np.random.uniform(-3, -0.5) if np.random.rand() < 0.5 else np.random.uniform(0.5, 3)
    hidden_nodes_index = list(range(adj_matrix.shape[0]-1,adj_matrix.shape[0]+len(hidden_nodes)-1))
    return new_adj_matrix, hidden_nodes_index



def methods_df_data(df_full, methods=['SubSearch','GreedySubSearch'], source_envs=[0], target_env=0, print_info=True, regularization=None, alpha=None , paras ={'SubSearch':{'valid_split':0.3,'delta':0.05, 'use_hsic':True},'GreedySubSearch':{'valid_split':0.5,'delta':0.05, 'use_hsic':True}}):
    """
    Run algorithms on data from dataframe given source and target environments information.
    
    Args:
        df_full: dataframe containing all the data
        methods: list of methods to run, supporting 'SubSearch', 'GreedySubSearch', 'CauPa', 'UseChild', 'UseMarkovBlanket', 'UseAll', 'UseAncestors'
        source_envs: list of source environments   
        target_env: target environment
        print_info: whether to print information
        regularization: None, 'lasso' or 'ridge'
        alpha: regularization parameter, default is 1.0
        paras: dictionary of parameters for special method: SubSearch and GreedySubSearch, parameters are also a dictionary
    
    Returns:
        MSEs of all the algorithms and selected features of each algorithm. A dataframe with columns ['method','MSE','features'] wit shape (len(methods),3).
    """
    
    X_train, y_train, X_test, y_test = select_df_data(df_full, source_envs, target_env)
    if print_info:
        print('X_train shape: {}, type: {}'.format(X_train.shape, type(X_train)))
        print('y_train shape: {}, type: {}'.format(y_train.shape, type(y_train)))
        print('X_test shape: {}, type: {}'.format(X_test.shape, type(X_test)))
        print('y_test shape: {}, type: {}'.format(y_test.shape, type(y_test)))
    # List of samples for all source environments, environment i corresponds to data_full['env']==i
    n_ex = [sum(df_full['env']==i) for i in source_envs]
    # create a dataframe to store the results
    results = pd.DataFrame(columns=['method','MSE','features'])
    
    # Run algorithms
    for method in methods:
        # print('Running method: {}'.format(method))
        if method == 'SubSearch':
            valid_split = paras['SubSearch']['valid_split']
            delta = paras['SubSearch']['delta']
            use_hsic = paras['SubSearch']['use_hsic']
            features = subset_search.subset(X_train, y_train, n_ex, valid_split=valid_split, delta=delta, use_hsic=use_hsic)
        elif method == 'GreedySubSearch':
            valid_split = paras['GreedySubSearch']['valid_split']
            delta = paras['GreedySubSearch']['delta']
            use_hsic = paras['GreedySubSearch']['use_hsic']
            features = subset_search.greedy_subset(X_train, y_train, n_ex, valid_split=valid_split, delta=delta, use_hsic=use_hsic)
        elif method == 'CauPa':
            baseSimulator = LinearDataSimulator(adj_matrix=df_full.base_model[0], noise_distributions=df_full.base_model[1])
            features = np.array(list(baseSimulator.get_causal_info(print_info=False)["parents"]))
        elif method == 'UseMarkovBlanket':
            baseSimulator = LinearDataSimulator(adj_matrix=df_full.base_model[0], noise_distributions=df_full.base_model[1])
            features = np.array(list(baseSimulator.get_causal_info(print_info=False)["markov_blanket"]))
        elif method == 'UseChildren':
            baseSimulator = LinearDataSimulator(adj_matrix=df_full.base_model[0], noise_distributions=df_full.base_model[1])
            features = np.array(list(baseSimulator.get_causal_info(print_info=False)["children"]))
        elif method == 'UseAncestors':
            baseSimulator = LinearDataSimulator(adj_matrix=df_full.base_model[0], noise_distributions=df_full.base_model[1])
            features = np.array(list(baseSimulator.get_causal_info(print_info=False)["ancestors"]))
        elif method == 'UseAll':
            features = np.arange(X_train.shape[1])
        else:
            raise ValueError('Method not supported')
        
        # Fit a model with features
        pred_y = fit_linear_model(X_train, y_train, X_test, y_test, feature_subset=features, regularization=regularization, alpha=alpha)
        # Calculate MSE
        mse = MSE(y_test=y_test, pred=pred_y)
        # Add results to the dataframe
        results = pd.concat([results, pd.DataFrame({'method':[method], 'MSE':[mse], 'features':[features]})], ignore_index=True)
    return results





def run_all_methods_dict(dict_full_list, methods, source_envs,target_env,target_test, repeats=10):
    """
    This function runs all the methods given in the 'methods' list across the specified source and target environments several times and calculates the mean squared error (MSE). The method uses the dictionary as input.
    
    Args:
    dict_full: a list of dictionary of data for all environments which has the same length as repeats.
    
    methods: list of methods to run.

    source_envs: list of source environments.

    target_envs: the target environment, type: int.

    target_test: extra simulation from target environment for testing, type: int.
    repeats: number of times to repeat the experiment.

    Returns:
    res_all: array of MSEs for all methods and all repeats, with shape (len(methods), repeats).

    res_all_test: array of MSEs for all methods and all repeats, with shape (len(methods), repeats).
    """
    res_all = np.zeros((len(methods), repeats))
    res_all_test = np.zeros((len(methods), repeats))

    # generate data 
    for repeat in range(repeats):
        for i, m in enumerate(methods):
            me = m.fit(dict_full_list[repeat], source=source_envs, target=target_env)
            xtar, ytar = dict_full_list[repeat][target_env]
            xtar_test, ytar_test = dict_full_list[repeat][target_test]
            targetE = MSE(ytar,me.predict(xtar))
            targetNE = MSE(ytar_test,me.predict(xtar_test))
            res_all[i, repeat] = targetE
            res_all_test[i, repeat] = targetNE
    return res_all, res_all_test




def repeat_all_on_a_DAG(repeats=1, n_environments=1, adj_matrix=np.zeros((1, 1)), distributions=[Distribution(np.random.normal, 0, 1, 0, 0.1)], n_sample=10, interventions=[{"type": None}], source_envs=[0], target_env=0, methods_use_dict=None, target_test=None,methods_use_df=None, regularization=None, alpha=0., df_paras=None):
    """ Repeats all prediction methods on a given DAG. May include interventions.
    Args:
        repeats (int): Number of times to repeat the experiment.

        n_environments (int): Number of environments to generate. Should have the same length as interventions.
        
        adj_matrix (np.array): Adjacency matrix of the DAG.
        
        distributions (list): List of noise distributions to use for each node. Should have the same length as adj_matrix.shape[0].
        
        n_sample (int): Number of samples to generate for each environment.
        
        interventions (list): List of interventions to apply. Each intervention is a dictionary with required keys.
        
        source_envs (list): List of source environments to train the models.
        
        target_env (int): target environments to test the models.

        methods_use_dict (list): List of methods to use on dictionaries. Each method should be a class.
        
        target_test (int): Target environment to test the methods_use_dict. If not specified, the target test environment is the same as the target environment.
        
        methods_use_df (list): List of methods to use on dataframes. Each method should be a string.
        
        regularization (str): Regularization method to use for df_methods. Should be 'None','lasso' or 'ridge'.
        
        alpha (float): Regularization parameter for 'lasso' or 'ridge'.
         
        df_paras (dict): Dictionary of parameters for df_methods: 'SubSearch' and 'GreedySubSearch'.

    Returns:
        res_all (np.array): Array of results for methods. Each row contains repeats for a method and each column contains a repeat for all methods.
        
        res_all_test (np.array): Array of results for methods where methods_use_dict is tested on test data. Each row contains repeats for a method and each column contains a repeat for all methods.

        names (list): List of names for methods. First names_use_dict, then names_use_df.

        names_short (list): List of short names for methods. First names_use_dict, then names_use_df.
        """
    # Create names for methods
    if regularization is not None and methods_use_df is not None:
        names_use_df = [m+'_'+regularization+'_'+str(alpha) for m in methods_use_df]
    elif methods_use_df is not None:
        names_use_df = methods_use_df
    else:
        names_use_df = []
    
    if methods_use_dict is not None:
        names_use_dict = [str(m) for m in methods_use_dict]
    else:
        names_use_dict = []
    
    names = names_use_dict + names_use_df
    names_short = [str(m).split('_')[0] for m in names]

    # Initialize variables
    res_all = None
    res_all_test = None
    all_data = []
    for i in range(repeats):
        # Generate simulations
        simulations = generate_simulations(n_environments=n_environments, adj_matrix=adj_matrix, noise_distributions=distributions, n_sample=n_sample, interventions=interventions)
        all_df_data = concatenate_data(simulations)
        all_df_data.base_model = [adj_matrix, distributions, [str(d) for d in distributions]]
        all_data.append(all_df_data)
        
        if methods_use_df is not None:
            if df_paras is None and ('SubSearch' in methods_use_df or 'GreedySubSearch' in methods_use_df):
                df_paras = {'SubSearch': {'valid_split': 0.3, 'delta': 0.05, 'use_hsic': True},
                            'GreedySubSearch': {'valid_split': 0.5, 'delta': 0.05, 'use_hsic': True}}
            results = methods_df_data(all_df_data, methods=methods_use_df, source_envs=source_envs, target_env=target_env, print_info=False, regularization=regularization, alpha=alpha, paras=df_paras)
            df_res_one = results['MSE'].values.reshape((-1, 1))
        else:
            df_res_one = None
        
        if methods_use_dict is not None:
            dict_full = df_to_dict(simulations)
            if target_test is None:
                target_test = target_env
            dict_res_one, dict_res_one_test = run_all_methods_dict([dict_full], methods=methods_use_dict, source_envs=source_envs, target_env=target_env, target_test=target_test, repeats=1)
        
        else:
            dict_res_one = None
            dict_res_one_test = None
        
        if methods_use_df is not None and methods_use_dict is not None:
            one_res_all = np.concatenate((dict_res_one, df_res_one), axis=0)
            one_res_all_test = np.concatenate((dict_res_one_test, df_res_one), axis=0)
        elif methods_use_df is not None:
            one_res_all = df_res_one
            one_res_all_test = df_res_one
        elif methods_use_dict is not None:
            one_res_all = dict_res_one
            one_res_all_test = dict_res_one_test
        
        if res_all is None:
            res_all = one_res_all
            res_all_test = one_res_all_test
        else:
            res_all = np.concatenate((res_all, one_res_all), axis=1)
            res_all_test = np.concatenate((res_all_test, one_res_all_test), axis=1)
    
    
    return res_all, res_all_test, names, names_short,all_data


def repeat_all_on_a_hidden_DAG(repeats=1, n_environments=1, adj_matrix_with_hidden=np.zeros((1, 1)), hidden_index = [] , distributions=[Distribution(np.random.normal, 0, 1, 0, 0.1)], n_sample=10, interventions=[{"type": None}], source_envs=[0], target_env=0, methods_use_dict=None, target_test=None,methods_use_df=None, regularization=None, alpha=0., df_paras=None):
    """ Repeats all prediction methods on a given DAG with hidden variables. May include interventions.
    Args:
        repeats (int): Number of times to repeat the experiment.

        n_environments (int): Number of environments to generate. Should have the same length as interventions.
        
        adj_matrix_with_hidden (np.array): Adjacency matrix of the DAG with hidden nodes.

        hidden_index (list): List of indices of hidden nodes.
        
        distributions (list): List of noise distributions to use for each node. Should have the same length as adj_matrix.shape[0].
        
        n_sample (int): Number of samples to generate for each environment.
        
        interventions (list): List of interventions to apply. Each intervention is a dictionary with required keys.
        
        source_envs (list): List of source environments to train the models.
        
        target_env (int): target environments to test the models.

        methods_use_dict (list): List of methods to use on dictionaries. Each method should be a class.
        
        target_test (int): Target environment to test the methods_use_dict. If not specified, the target test environment is the same as the target environment.
        
        methods_use_df (list): List of methods to use on dataframes. Each method should be a string.
        
        regularization (str): Regularization method to use for df_methods. Should be 'None','lasso' or 'ridge'.
        
        alpha (float): Regularization parameter for 'lasso' or 'ridge'.
         
        df_paras (dict): Dictionary of parameters for df_methods: 'SubSearch' and 'GreedySubSearch'.

    Returns:
        res_all (np.array): Array of results for methods. Each row contains repeats for a method and each column contains a repeat for all methods.
        
        res_all_test (np.array): Array of results for methods where methods_use_dict is tested on test data. Each row contains repeats for a method and each column contains a repeat for all methods.

        names (list): List of names for methods. First names_use_dict, then names_use_df.

        names_short (list): List of short names for methods. First names_use_dict, then names_use_df.
        """
    # Create names for methods
    if regularization is not None and methods_use_df is not None:
        names_use_df = [m+'_'+regularization+'_'+str(alpha) for m in methods_use_df]
    elif methods_use_df is not None:
        names_use_df = methods_use_df
    else:
        names_use_df = []
    
    if methods_use_dict is not None:
        names_use_dict = [str(m) for m in methods_use_dict]
    else:
        names_use_dict = []
    
    names = names_use_dict + names_use_df
    names_short = [str(m).split('_')[0] for m in names]

    # Initialize variables
    res_all = None
    res_all_test = None
    all_data = []
    for i in range(repeats):
        # Generate simulations
        simulations = generate_simulations(n_environments=n_environments, adj_matrix=adj_matrix_with_hidden, noise_distributions=distributions, n_sample=n_sample, interventions=interventions)
        all_df_data = concatenate_data_with_hidden(simulations, hidden_indices=hidden_index)
        #print(all_df_data)
        # generate adjaceny matrix without hidden variables
        adj_matrix_without_hidden = np.delete(adj_matrix_with_hidden, hidden_index, axis=0)
        adj_matrix_without_hidden = np.delete(adj_matrix_without_hidden, hidden_index, axis=1)
        #print(adj_matrix_without_hidden)
        # generate noise distributions without hidden variables
        distributions_without_hidden = [distributions[i] for i in range(len(distributions)) if i not in hidden_index]

        all_df_data.base_model = [adj_matrix_without_hidden, distributions_without_hidden, [str(d) for d in distributions_without_hidden]]
        
        all_df_data.true_base_model = [adj_matrix_with_hidden, distributions, [str(d) for d in distributions]]
        all_data.append(all_df_data)
        
        if methods_use_df is not None:
            if df_paras is None and ('SubSearch' in methods_use_df or 'GreedySubSearch' in methods_use_df):
                df_paras = {'SubSearch': {'valid_split': 0.3, 'delta': 0.05, 'use_hsic': True},
                            'GreedySubSearch': {'valid_split': 0.5, 'delta': 0.05, 'use_hsic': True}}
            results = methods_df_data(all_df_data, methods=methods_use_df, source_envs=source_envs, target_env=target_env, print_info=False, regularization=regularization, alpha=alpha, paras=df_paras)
            df_res_one = results['MSE'].values.reshape((-1, 1))
        else:
            df_res_one = None
        
        # index of variables that are not hidden
        nonhidden_x_indices = [i for i in range(len(adj_matrix_with_hidden)-1) if i not in hidden_index]
        if methods_use_dict is not None:
            dict_full = {}
            for i in range(len(simulations)):
                dict_full[simulations[i].env] = (np.array(simulations[i].iloc[:,nonhidden_x_indices]), np.array(simulations[i].iloc[:,-1]))
            if target_test is None:
                target_test = target_env
            dict_res_one, dict_res_one_test = run_all_methods_dict([dict_full], methods=methods_use_dict, source_envs=source_envs, target_env=target_env, target_test=target_test, repeats=1)
        
        else:
            dict_res_one = None
            dict_res_one_test = None
        
        if methods_use_df is not None and methods_use_dict is not None:
            one_res_all = np.concatenate((dict_res_one, df_res_one), axis=0)
            one_res_all_test = np.concatenate((dict_res_one_test, df_res_one), axis=0)
        elif methods_use_df is not None:
            one_res_all = df_res_one
            one_res_all_test = df_res_one
        elif methods_use_dict is not None:
            one_res_all = dict_res_one
            one_res_all_test = dict_res_one_test
        
        if res_all is None:
            res_all = one_res_all
            res_all_test = one_res_all_test
        else:
            res_all = np.concatenate((res_all, one_res_all), axis=1)
            res_all_test = np.concatenate((res_all_test, one_res_all_test), axis=1)
    
    
    return res_all, res_all_test, names, names_short,all_data










def boxplot_all_methods(res_all, method_index, title='', names=[], COLOR_PALETTE=sns.color_palette(n_colors=10,desat=1.), figsize =(10,6),ylim_option=0): 
    """plots boxplots for all methods in res_all.

    Args:
    res_all: array of MSEs for all methods and all repeats, with shape (len(methods), repeats).
    method_index: list of indices of methods to plot.
    title: title of the plot.
    names: list of names for all methods.
    COLOR_PALETTE: color palette for the plot.
    figsize: figure size.
    ylim_option: 0 for no ylim, 1 for ylim=(0,1), 2 for ylim=(0,2), 3 for ylim=(0,3), 4 for ylim=(0,4), 5 for ylim=(0,5), 6 for ylim=(0,6), 7 for ylim=(0,7), 8 for ylim=(0,8), 9 for ylim=(0,9), 10 for ylim=(0,10).

    Returns:
    fig: figure object.
    """
    _ , ax = plt.subplots(figsize=figsize)
    res_all_df = pd.DataFrame(res_all.T)
    res_all_df.columns = names
    select_df = res_all_df.iloc[:, method_index]
    select_df_melt = select_df.melt(var_name='methods', value_name='MSE')
    res_all_mean = np.mean(res_all, axis=1)
    
    ax.set_title(title, fontsize=20)

    # ax.axhline(res_all_mean[1], ls='--', color='b')
    ax.axhline(res_all_mean[0], ls='--', color='r')
    ax = sns.boxplot(x="methods", y="MSE", data=select_df_melt,
                     palette=[COLOR_PALETTE[i] for i in np.arange(len(method_index))],
                     ax=ax)
    ax.set_xticklabels(ax.get_xticklabels(), rotation=-70, ha='left', fontsize=15)
    ax.tick_params(labelsize=15)
#     ax.yaxis.set_major_formatter(matplotlib.ticker.FormatStrFormatter('%.2f'))
    ax.yaxis.grid(False) # Hide the horizontal gridlines
    ax.xaxis.grid(True) # Show the vertical gridlines
#     ax.xaxis.set_visible(False)
    ax.set_xlabel("")
    ax.set_ylabel("MSE", fontsize=15)

    if ylim_option == 0:
        lower_ylim = res_all_mean[0] -  (res_all_mean[1] - res_all_mean[0]) *0.3
        # upper_ylim = max(res_all_mean[1] + (res_all_mean[1] - res_all_mean[0]) *0.3, res_all_mean[0]*1.2)
        upper_ylim = res_all_mean[1] + (res_all_mean[1] - res_all_mean[0]) *0.3
        # get the boxes that are outside of the plot
        outside_index = np.where(res_all_mean >  upper_ylim)[0]
        for oindex in outside_index:
            ax.annotate("box\nbeyond\ny limit", xy=(oindex - 0.3, upper_ylim - (upper_ylim-lower_ylim)*0.15 ), fontsize=15)
        ax.set_ylim(lower_ylim, upper_ylim)
    else: 
        ax.set_ylim(0, ylim_option)


def scatterplot_two_methods(res_all, index1=0, index2=1, names=[], colors=sns.color_palette(n_colors=10,desat=1.)[0], title="", figsize= (10,6), ylimmax = 1.1):
    """Scatterplot two methods selected in res_all
    Args:
        res_all: a list of results from all methods
        index1: the index of the first method in res_all
        index2: the index of the second method in res_all
        names: a list of names for all methods
        colors: a list of colors for the two methods
        title: the title of the plot
        figsize: the size of the plot
        ylimmax: the max of y axis
    Returns:
        None
    """
    _ , ax = plt.subplots(figsize=figsize) 
    ax.scatter(res_all[index1], res_all[index2], alpha=1.0, marker='+', c = np.array(colors).reshape(1, -1), s=100)
    ax.set_xlabel(names[index1], fontsize=20)
    ax.set_ylabel(names[index2], fontsize=20)
    ax.tick_params(labelsize=20)
    
    if ylimmax <= 0:
        # set ylim automatically
        # ylimmax = np.max((np.max(res_all[index1]), np.max(res_all[index2])))
        ylimmax = np.percentile(np.concatenate((res_all[index1], res_all[index2])), 95)
        print("ylimmax automatically is set to %.3f" %ylimmax)
    # 
    ax.plot([0, ylimmax],[0, ylimmax], 'k--', alpha=0.5)
    # plt.axis('equal')
    ax.set_xlim(0.0, ylimmax)
    ax.set_ylim(0.0, ylimmax)
    ax.set_title(title, fontsize=20)





class DAGGenerator():
    """ 
    Class to generate random DAGs which have no isolated nodes. The DAGs have type 'mixed' or 'causal' or 'anticausal'. 
    
    p is the number of nodes in the DAG.
    """
    def __init__(self):
        pass

    def generate(self,p,type='mixed'):
        """generates a random DAG with p nodes using method generate_one_dag().
          type can be 'mixed' or 'causal' or 'anticausal'"""
        self.p = p
        # Creating list of nodes.
        nodes = [f'X{i+1}' for i in range(self.p-1)]
        nodes.append('Y')
        
        if type == 'mixed':
            adj_matrix = np.zeros([self.p,self.p])
            # repetively generate a DAG until Y has at least one parent and one child
            while(np.sum(adj_matrix[:,self.p-1])==0 or np.sum(adj_matrix[self.p-1,:])==0):
                adj_matrix = self.generate_one_dag(self.p)
        elif type == 'causal':
            adj_matrix = np.zeros([self.p,self.p])
            adj_matrix[:-1,:-1] = self.generate_one_dag(self.p-1)
            # randomly selects at least one node from range(p-1) to be a parent of y
            adj_matrix[np.random.choice(self.p-1,np.random.randint(1,self.p-1),replace=False),-1] = 1
        elif type == 'anticausal':
            adj_matrix = np.zeros([self.p,self.p])
            adj_matrix[:-1,:-1] = self.generate_one_dag(self.p-1)
            # randomly selects at least one node from range(p-1) to be a child of y
            adj_matrix[-1,np.random.choice(self.p-1,np.random.randint(1,self.p-1),replace=False)] = 1
        else:
            raise ValueError("type must be 'mixed' or 'causal' or 'anticausal'")
        # Assign random weights to each nonzero adjacency matrix element with a uniformly samples number from [-3,-0.5] U [0.5,3]
        for i in range(self.p):
            for j in range(self.p):
                if adj_matrix[i, j] != 0:
                    adj_matrix[i, j] = np.random.uniform(-3, -0.5) if np.random.rand() < 0.5 else np.random.uniform(0.5, 3)
        return adj_matrix, nodes
    
    def generate_one_dag(self,p):
        """generates one random DAG with p nodes, ensuring that there are no isolated nodes."""
        adj_matrix = np.zeros((p,p), dtype=float)
        while(nx.number_of_isolates(nx.DiGraph(adj_matrix)) != 0):
            # Creating edges to ensure DAG has no isolated nodes.
            for i in range(p-1):
                for j in range(i+1,p):
                    if np.random.rand() < 0.5:
                        adj_matrix[i, j] = 1
                    else:
                        adj_matrix[j, i] = 1
                # Remove any back edges using topological sort and check for cycles.
                try:
                    sorted_indices = np.argsort(np.sum(adj_matrix, axis=1)) # Topological sort
                    for k in sorted_indices:
                        ind = np.where(adj_matrix[k,:]>0)[0]
                        adj_matrix[k, ind] = 0
                        if not self.cycle_exists(adj_matrix):
                            break
                        adj_matrix[k, ind] = 1
                except ValueError:
                    pass # Already a cycle
        return adj_matrix
    
    def cycle_exists(self, graph):
        """Return True if a cycle exists in the given graph"""
        visited = [False]*graph.shape[0]
        for i in range(graph.shape[0]):
            if not visited[i]:
                if self.has_cycle(i, visited, graph):
                    return True
        return False
    
    def has_cycle(self, node, visited, graph):
        """Helper function for cycle detection using DFS"""
        visited[node] = True
        for i in range(graph.shape[0]):
            if graph[node, i] == 1 and not visited[i]:
                if self.has_cycle(i, visited, graph):
                    return True
            elif graph[node, i] == 1 and visited[i]:
                return True
        visited[node] = False
        return False

def get_edges_from_matrix(adj_matrix:np.ndarray):
    """Returns a list of edges from an adjacency matrix using networkx"""
    G = nx.DiGraph(adj_matrix)
    return list(G.edges)


def generate_dags(num_dags=1, p_scale=[3]):
    """
    Generate a specified number of DAGs for each value of p in p_scale
    p_scale is a list of integers specifying the number of nodes we want.
    """
    dags = []
    for p in p_scale:
        for i in range(num_dags):
            dag = DAGGenerator()
            adj_matrix, nodes = dag.generate(p)
            dags.append((adj_matrix, nodes))
    return dags


def plot_example_dags(p_scale=[3]):
    """
    Plot one example DAG for each value of p in p_scale using networkx.draw_networkx
    p_scale is a list of integers specifying the number of nodes we want.
    """
    dags = generate_dags(num_dags=1, p_scale=p_scale)
    for i, p in enumerate(p_scale):
        adj_matrix, nodes = dags[i]
        G = nx.DiGraph(adj_matrix)
        pos = nx.circular_layout(G)
        node_color = ['silver' if i != nodes.index('Y') else 'r' for i in range(len(nodes))]
        nx.draw_networkx(G, pos, labels={i: nodes[i] for i in range(len(nodes))}, with_labels=True, node_color=node_color)
        plt.box(False)
        plt.show()


def check_dags(dags):
    """Check if all DAGs are acyclic. dags can be the output of the function generate_dags."""
    for adj_matrix, _ in dags:
        G = nx.DiGraph(adj_matrix)
        if not nx.is_directed_acyclic_graph(G):
            print("Found a cyclic graph!")
            return False
    print("All DAGs are acyclic!")
    return True


def plot_generated_dag(adj_matrix, nodes, hidden_nodes=None):
    """Plot a DAG using networkx.draw_networkx.
    adj_matrix is the adjacency matrix of the DAG.
    nodes is a list of node names."""
    # plot the generated DAG
    G = nx.DiGraph(adj_matrix)
    pos = nx.circular_layout(G)
    # plot a graph with red node for y and all other nodes in silver
    node_color = ['silver' if i != nodes.index('Y') else 'red' for i in range(len(nodes))]
    # if hidden_nodes is None, plot the DAG without hidden nodes
    if hidden_nodes is None:
        nx.draw_networkx(G, pos, labels={i: nodes[i] for i in range(len(nodes))}, with_labels=True, node_color=node_color)
    else:
        # if hidden_nodes is not None, plot the DAG with hidden nodes and hidden nodes have color 'grey', Y has color 'red', rest have color 'silver'
        node_color = ['silver' for i in range(len(nodes))]
        for j in hidden_nodes:
            node_color[j] = 'grey'
        node_color[nodes.index('Y')] = 'red'
        nx.draw_networkx(G, pos, labels={i: nodes[i] for i in range(len(nodes))}, with_labels=True, node_color=node_color)
    plt.box(False)
    plt.show()

    