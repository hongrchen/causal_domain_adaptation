import numpy as np
from numpy.linalg import inv
from numpy.linalg import pinv
import pandas as pd
import networkx as nx
import matplotlib.pyplot as plt
from copy import deepcopy




class Distribution():
    """ This class is used to represent a probability distribution object with a distribution type and associated parameters. The 'sample' method can be used to generate samples from the distribution using the parameters defined in the object. 
    
    Args:   
        distribution: The distribution type. It should be a numpy function that takes the parameters as arguments.
        mean_shift: The mean shift of the distribution. The default value is 0.
        variance_shift: The variance shift of the distribution. The default value is 1.
        *parameters: The parameters of the distribution. The number of parameters should be the same as the number of arguments of the distribution type."""
    def __init__(self,distribution,mean_shift=0,variance_shift=1,*parameters) -> None:
        self.distribution = distribution
        self.parameters = parameters
        self.mean_shift = mean_shift
        self.variance_shift = variance_shift
    
    def sample(self):
        return self.variance_shift*self.distribution(*self.parameters) + self.mean_shift
    
    def __str__(self) -> str:
        return f"{self.distribution.__name__}, mean_shift:{self.mean_shift}, variance_shift:{self.variance_shift}, parameters:{self.parameters}"


def generate_simulations(n_environments, adj_matrix, noise_distributions, n_sample, interventions):
    """
    Generate simulations for multiple environments.

    Parameters:
        n_environments: Number of environments to generate simulations for
        adj_matrix: The adjacency matrix of the graph
        noise_distributions: List of noise distributions for each variable
        n_sample: Number of samples to be generated for each environment
        interventions: List of dictionaries containing intervention info for each environment
                    Example: [{"type": "do", "intervene_Y": 1, "intervention_nodes": [0, 1], "new_distribution": Distribution(np.random.normal,0,1,0,1)},
                            {"type": "mean_shift", "intervene_Y": 2, "intervention_nodes": [1, 2], "generator": Distribution(np.random.uniform,0,1,-1,1)}]
    
    Returns:
        simulations: List of simulations for each environment as pandas dataframes
    """
    simulations = []
    lds = LinearDataSimulator(adj_matrix=adj_matrix, noise_distributions=noise_distributions, n_sample=n_sample)
    for i in range(n_environments):
        intervention = interventions[i]
        lds_copy = deepcopy(lds)
        if intervention["type"] is None:
            pass
        elif intervention["type"] == "do":
            lds_copy.intervention_do(intervene_Y=intervention["intervene_Y"], intervention_nodes=intervention["intervention_nodes"], new_distribution=intervention["new_distribution"])
        elif intervention["type"] == "mean_shift":
            lds_copy.intervention_mean_shift(intervene_Y=intervention["intervene_Y"], intervention_nodes=intervention["intervention_nodes"], generator=intervention["generator"])
        elif intervention["type"] == "variance_shift":
            lds_copy.intervention_variance_shift(intervene_Y=intervention["intervene_Y"], intervention_nodes=intervention["intervention_nodes"], generator=intervention["generator"])
        elif intervention["type"] == "distribution_shift":
            lds_copy.intervention_distribution_shift(intervene_Y=intervention["intervene_Y"], intervention_nodes=intervention["intervention_nodes"], new_distribution=intervention["new_distribution"])
        elif intervention["type"] == "causal_effects":
            lds_copy.intervention_causal_effects(intervene_Y=intervention["intervene_Y"], intervention_nodes=intervention["intervention_nodes"], scaling_factor=intervention["scaling_factor"])
        simulation = lds_copy.simulate(env=i)
        simulations.append(simulation)
    return simulations


class NonlinearDataSimulator():
    """Relations define the relations between the variables. It is a list of functions. Each function takes two arguments: the first is a list of the values of the variables in the previous step, and the second is the noise.
    
    The noise is generated from the noise_distributions. It is a list of Distribution objects.
    
    n_sample is the number of samples to be generated.

    The generate_order is a list of integers that defines the order of generating the variables.
    """
    def __init__(self,
                 relations,
                 noise_distributions,
                 n_sample,
                 generate_order,
                 ) -> None:
        assert len(relations) == len(noise_distributions) == len(generate_order), "The number of relations, noise distributions and generate order should be the same."
        self.relations = relations
        self.noise_distributions = noise_distributions
        self.n = n_sample
        self.generate_order = generate_order
        self.p = len(relations)
    
    def simulate(self,env=0):
        """ env is the environment number. Returns a pandas dataframe with the generated data. """
        data = np.zeros((self.n,self.p))
        for i in range(self.n):
            for j in self.generate_order:
                data[i,j] = self.relations[j](data[i,:],self.noise_distributions[j].sample())
        df = pd.DataFrame(data)
        df.columns = [f"X{j+1}" for j in range(self.p-1)] + ["Y"]
        df.env = env
        return df



class LinearDataSimulator():
    """ linear version for the data simulator.
    
    adj_matrix is the adjacency matrix of the graph. It is a numpy array.
    
    noise_distributions is a list of Distribution objects. It is the noise distribution for each variable.
    
    n_sample is the number of samples to be generated.
    """
    def __init__(self,
                 adj_matrix,
                 noise_distributions,
                 n_sample=1) -> None:
        assert adj_matrix.shape[0] == len(noise_distributions), "The number of relations, noise distributions and generate order should be the same."
        self.adj_matrix = adj_matrix
        self.noise_distributions = noise_distributions
        self.n = n_sample
        self.p = self.adj_matrix.shape[0]
        # intervention_info stores each intervention applied to the DAG
        self.intervention_info = None
        self.nodes = [f'X{i+1}' for i in range(self.p-1)]
        self.nodes.append('Y')
        self.intervention_nodes_all = []
    
    def simulate(self,env=0):
        """ env is the environment number. Returns a pandas dataframe with the generated data. """
        data = np.zeros((self.n,self.p))
        for i in range(self.n):
            noise = np.array([self.noise_distributions[j].sample() for j in range(self.p)])
            data[i,:] = pinv(np.eye(self.p) - self.adj_matrix.T)@noise
        df = pd.DataFrame(data)
        df.columns = [f"X{j+1}" for j in range(self.p-1)] + ["Y"]
        df.env = env
        df.intervention_info = self.intervention_info
        df.intervention_nodes_all = self.intervention_nodes_all
        return df
    
    def intervention_do(self,intervene_Y,intervention_nodes=[],new_distribution=Distribution(np.random.normal,0,1,0,1)):
        """ deletes edges that point to the intervened variable.

        intervene_Y can be chosen from [0,1,2].
        0: Y is not included in the intervention.
        1: only Y is included in the intervention.
        2: Y and some Xs are included in the intervention.

        intervention_nodes is a list of variables that are included in the intervention.

        all the intervened variables will be set to the new distribution.
        """
        if new_distribution is None:
            new_distribution = Distribution(np.random.normal,0,1,0,1)
        # generates the intervention nodes based on the input parameters
        intervention_nodes_all = self._generate_intervention_nodes(intervene_Y,intervention_nodes)
        self.intervention_nodes_all = intervention_nodes_all
        self._delete_edges(intervention_nodes_all)
        # adds the new constant values to the intervened variables
        for i in range(len(intervention_nodes_all)):
            self.noise_distributions[intervention_nodes_all[i]] = new_distribution
        # stores the intervention information
        self.intervention_info = f'do_intervention: intervention_nodes={self._generate_symbolic_nodes(intervention_nodes_all)}, new_distribution={str(new_distribution)}'
    
    def intervention_mean_shift(self,intervene_Y,intervention_nodes=[],generator=Distribution(np.random.uniform,0,1,-1,1)):
        """ shifts the noise mean of the intervened variables. """
        if generator is None:
            generator = Distribution(np.random.uniform,0,1,-1,1)
        # generates the intervention nodes based on the input parameters
        intervention_nodes_all = self._generate_intervention_nodes(intervene_Y,intervention_nodes)
        self.intervention_nodes_all = intervention_nodes_all
        # shifts the mean of the intervened variables
        shifts = []
        for node in intervention_nodes_all:
            shift = generator.sample()
            self.noise_distributions[node].mean_shift = shift
            shifts.append(shift)
        # stores the intervention information
        self.intervention_info = f'mean_shift: intervention_nodes={self._generate_symbolic_nodes(intervention_nodes_all)}, mean_shift={shifts}'
    
    def intervention_variance_shift(self,intervene_Y,intervention_nodes=[],generator=Distribution(np.random.uniform,0,1,0.5,2)):
        """ shifts the noise variance of the intervened variables. """
        if generator is None:
            generator = Distribution(np.random.uniform,0,1,0.5,2)

        # generates the intervention nodes based on the input parameters
        intervention_nodes_all = self._generate_intervention_nodes(intervene_Y,intervention_nodes)
        self.intervention_nodes_all = intervention_nodes_all
        # shifts the variance of the intervened variables
        shifts = []
        for node in intervention_nodes_all:
            shift = generator.sample()
            self.noise_distributions[node].variance_shift = shift
            shifts.append(shift)
        # stores the intervention information
        self.intervention_info = f'variance_shift: intervention_nodes={self._generate_symbolic_nodes(intervention_nodes_all)}, variance_shift={shifts}'
    
    def intervention_distribution_shift(self,intervene_Y,intervention_nodes=[],new_distribution=Distribution(np.random.normal,0,1,0,1)):
        """ shifts the noise distribution of the intervened variables. """
        if new_distribution is None:
            new_distribution = Distribution(np.random.normal,0,1,0,1)
        # generates the intervention nodes based on the input parameters
        intervention_nodes_all = self._generate_intervention_nodes(intervene_Y,intervention_nodes)
        self.intervention_nodes_all = intervention_nodes_all
        # changes the distribution of the intervened variables
        for node in intervention_nodes_all:
            self.noise_distributions[node] = new_distribution
        # stores the intervention information
        self.intervention_info = f'distribution_shift: intervention_nodes={self._generate_symbolic_nodes(intervention_nodes_all)}, new_distribution={str(new_distribution)}'
    
    def intervention_causal_effects(self,intervene_Y,intervention_nodes=[],scaling_factor=0.5):
        # generates the intervention nodes based on the input parameters
        if scaling_factor is None:
            scaling_factor = 0.5
        intervention_nodes_all = self._generate_intervention_nodes(intervene_Y,intervention_nodes)
        self.intervention_nodes_all = intervention_nodes_all
        for node in intervention_nodes_all:
            self.adj_matrix[:,node] = self.adj_matrix[:,node]*scaling_factor
        # stores the intervention information
        self.intervention_info = f'causal_effect_intervention: intervention_nodes={self._generate_symbolic_nodes(intervention_nodes_all)}, scaling_factor={scaling_factor}'
    
    def _generate_intervention_nodes(self,intervene_Y,intervention_nodes):
        """ generates the intervention nodes based on the input parameters. """
        int_temp = deepcopy(intervention_nodes)
        if intervene_Y == 0:
            if int_temp == []:
                # randomly adds multiple (at least one) nodes to the intervention set
                int_temp = np.random.choice(self.p-1,size=np.random.randint(1,self.p-1),replace=False).tolist()
        elif intervene_Y == 1:
            int_temp = [self.p-1]
        elif intervene_Y == 2:
            if int_temp == []:
                # randomly adds multiple (at least one) nodes to the intervention set
                int_temp = np.random.choice(self.p-1,size=np.random.randint(1,self.p-1),replace=False).tolist()
            int_temp.append(self.p-1)
        int_temp.sort()
        return int_temp
    
    def _generate_symbolic_nodes(self,intervention_nodes):
        """ converts the intervention nodes to the symbolic form. """
        symbolic_intervention_nodes = []
        for node in intervention_nodes:
            if node == self.p-1:
                symbolic_intervention_nodes.append("Y")
            else:
                symbolic_intervention_nodes.append(f"X{node+1}")
        return symbolic_intervention_nodes

    def _delete_edges(self,intervention_nodes):
        """ deletes edges that point to the intervened variable. """
        for node in intervention_nodes:
            self.adj_matrix[:,node] = 0
    
    def get_causal_info(self,print_info=False):
        """ Returns the causal information of the graph as a dictionary. """
        causal_info = {}
        # parents is the set of parents of the last variable (Y)
        parents = set(np.flatnonzero(self.adj_matrix[:,self.p-1]))
        # children is the set of sons of the last variable (Y)
        children = set(np.flatnonzero(self.adj_matrix[self.p-1,:]))
        # spouses is the set of variables that are the parents of Y's children
        spouses = set()
        for child in children:
            spouses = spouses.union(set(np.flatnonzero(self.adj_matrix[:,child])))
        spouses = spouses.difference(set([self.p-1]))
        # ancestors is the set of all the nodes that have a directed edge to Y
        G = nx.DiGraph(self.adj_matrix)
        ancestors = nx.ancestors(G,self.p-1)
        # markov_blanket is the set of parents, children and spouses of Y
        markov_blanket = parents.union(children).union(spouses)

        causal_info['parents'] = parents
        causal_info['children'] = children
        causal_info['spouses'] = spouses
        causal_info['ancestors'] = ancestors
        causal_info['markov_blanket'] = markov_blanket

        if print_info:
            # prints the parents of Y, converts integers to strings
            print("Parents of Y: ", [f"X{j+1}" for j in parents])
            # prints the children of Y, converts integers to strings
            print("Children of Y: ", [f"X{j+1}" for j in children])
            # prints the spouses of Y, converts integers to strings
            print("Spouses of Y: ", [f"X{j+1}" for j in spouses])
            # prints the ancestors of Y, converts integers to strings
            print("Ancestors of Y: ", [f"X{j+1}" for j in ancestors])
            # prints the markov blanket of Y, converts integers to strings
            print("Markov blanket of Y: ", [f"X{j+1}" for j in markov_blanket])
        else:
            pass
        return causal_info







def draw_simulation_model(adj_matrix, layout='circular', figsize=(10, 6),invariantlist=[],intervenelist=[],hiddenlist = [], weight = False):
    """ Plot the simulation DAG model.
    
    Args:
        adj_matrix (numpy.ndarray): adjacency matrix of the DAG.
        layout (str): layout of the graph. Options are 'circular', 'spring', 'kamada_kawai'.
        figsize (tuple): figure size. Example: (10, 6).
        invariantlist (list): list of invariant nodes.
        intervenelist (list): list of intervened nodes.
        hiddenlist (list): list of hidden nodes.
        
    Returns:
        None"""
    plt.figure(figsize=figsize)
    G = nx.DiGraph()
    p = adj_matrix.shape[0]
    G.add_nodes_from(np.arange(p))
    for i in range(p):
        for j in range(p):
            if adj_matrix[i, j] != 0:
                G.add_edge(i, j, weight=adj_matrix[i, j])
    # labels
    labels={}
    temp1 = 1
    temp2 = 1
    for i in range(p-1):
        if i not in hiddenlist:
            labels[i] = "X"+str(temp1)
            temp1 += 1
        else:
            labels[i] = "H"+str(temp2)
            temp2 += 1

    labels[p-1] = "Y"
    # position
    if layout=="spring":
        pos = nx.spring_layout(G)
    elif layout=="kamada_kawai":
        pos = nx.kamada_kawai_layout(G)
    else:
        pos = nx.circular_layout(G)
        
    nx.draw_networkx_edges(G,pos,width=2.0,alpha=1, arrows=True, arrowsize=20, node_size=1000, arrowstyle='-|>')

    nx.draw_networkx_nodes(G,pos,nodelist = list(np.arange(p-1)), node_color='lightgrey', node_size=1000, alpha=0.8)
    nx.draw_networkx_nodes(G,pos,nodelist = [p-1], node_color='r',
        node_size=1000, alpha=0.8)
    
    nx.draw_networkx_nodes(G,pos,nodelist = list(invariantlist), node_color='y', node_size=1000, alpha=0.8)

    nx.draw_networkx_nodes(G,pos,nodelist = list(intervenelist), node_color='g', node_size=1000, alpha=0.8)

    nx.draw_networkx_nodes(G,pos,nodelist = list(hiddenlist), node_color='dimgrey', node_size=1000, alpha=0.8)
    
    if weight:
        arc_weight=nx.get_edge_attributes(G,'weight')
        arc_weight_format = {i:'{:.2f}'.format(arc_weight[i]) for i in arc_weight}
        nx.draw_networkx_edge_labels(G, pos,font_color= 'k', edge_labels=arc_weight_format, font_size=10)
    
    nx.draw_networkx_labels(G,pos,labels,font_size=17)
    plt.box(False)
    plt.draw()
    plt.show()