# Causal_Domain_Adaptation



## Introduction

Domain adaptation aims at learning a prediction model using labeled training data from
one or multiple source domains, which can perform well on the unlabeled test data in a target domain generated from a distribution that differs from the source domain distributions.

We propose a unified practical causal framework based on structural causal models, which
enables the analysis and comparison of various domain adaptation methods, providing a
comprehensive understanding of their strengths and limitations. Our framework
facilitates efficient model modifications, allowing for the incorporation of hidden variables, alterations to interventions, modifications of causal structures, and changes in noise distributions. Furthermore, we modify the current causal structure learning methods for domain adaptation. Through experiments over various model settings, we assess the effectiveness and limitations of different algorithms.

## Code Structure

Explanation of each subfolder:

- data_processed/: This folder contains processed data that is intended for use in R. 

- data_raw/: This folder contains raw data files that are intended for use in Python. 

- figures/: This folder is used to store any generated figures or visualizations that are created during the execution of code. 

- Python/: This folder contains the implementation of the main framework in Python. It includes all the Python code files necessary for the project. 
    - SCM.ipynb is a Jupyter notebook file that provides a user-friendly interface for using and modifying the SCM (Structural Causal Models) framework. Users can open this notebook and make necessary modifications according to their specific needs.

- R/: This folder contains the code for evaluating methods written in R. 
    - DomainAdaptation.Rmd is an R Markdown file that provides a user-friendly interface for evaluateing the methods written in R. It can perform domain adaptation tasks and save the results to the data_raw/ folder for further processing or analysis.